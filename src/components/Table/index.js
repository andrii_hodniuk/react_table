import React from 'react';

export default props => (
	<table className="table">
		<thead>
		<tr>
			<th onClick={props.onSort.bind(null, 'ID')}>id {props.sortField === 'ID' ?
				<small>{props.sort}</small> : null}</th>
			<th onClick={props.onSort.bind(null, 'FIRSTnAME')}>First Name {props.sortField === 'FIRSTnAME' ?
				<small>{props.sort}</small> : null}</th>
			<th onClick={props.onSort.bind(null, 'lastName')}>Last Name {props.sortField === 'lastName' ?
				<small>{props.sort}</small> : null}</th>
			<th onClick={props.onSort.bind(null, 'email')}>Email {props.sortField === 'email' ?
				<small>{props.sort}</small> : null}</th>
			<th onClick={props.onSort.bind(null, 'phone')}>Phone {props.sortField === 'phone' ?
				<small>{props.sort}</small> : null}</th>
		</tr>
		</thead>
		<tbody>
		{props.data.map(item => (
			<tr key={`${item.ID}_${item.phone}`} onClick={props.onRowSelect.bind(null, item)}>
				<td>{item.ID}</td>
				<td>{item.FIRSTnAME}</td>
				<td>{item.lastName}</td>
				<td>{item.email}</td>
				<td>{item.phone}</td>
			</tr>
		))}
		</tbody>
	</table>
)